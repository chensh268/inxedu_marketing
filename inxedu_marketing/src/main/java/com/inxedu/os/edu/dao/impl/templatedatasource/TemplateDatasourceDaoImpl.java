package com.inxedu.os.edu.dao.impl.templatedatasource;

import com.inxedu.os.common.dao.GenericDaoImpl;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.templatedatasource.TemplateDatasourceDao;
import com.inxedu.os.edu.entity.templatedatasource.TemplateDatasource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板动态数据来源 TemplateDatasourceDao接口实现
 */
@Repository("templateDatasourceDao")
public class TemplateDatasourceDaoImpl extends GenericDaoImpl implements TemplateDatasourceDao{
	/**
     * 添加模板动态数据来源
     */
    public void addTemplateDatasource(TemplateDatasource templateDatasource){
    	this.insert("TemplateDatasourceMapper.addTemplateDatasource", templateDatasource);
    }
    
    /**
     * 删除模板动态数据来源
     * @param dataKey
     */
    public void delTemplateDatasourceByDataKey(Long dataKey){
    	this.update("TemplateDatasourceMapper.delTemplateDatasourceByDataKey", dataKey);
    }
    
    /**
     * 修改模板动态数据来源
     * @param templateDatasource
     */
    public void updateTemplateDatasource(TemplateDatasource templateDatasource){
    	this.update("TemplateDatasourceMapper.updateTemplateDatasource", templateDatasource);
    }
    
    /**
     * 通过dataKey，查询模板动态数据来源
     * @param dataKey
     * @return
     */
    public TemplateDatasource getTemplateDatasourceByDataKey(Long dataKey){
    	return this.selectOne("TemplateDatasourceMapper.getTemplateDatasourceByDataKey", dataKey);
    }
    
    /**
     * 分页查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @param page 分页条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceListPage(TemplateDatasource templateDatasource,PageEntity page){
    	return this.queryForListPage("TemplateDatasourceMapper.queryTemplateDatasourceListPage", templateDatasource, page);
    }
    
    /**
     * 条件查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceList(TemplateDatasource templateDatasource){
    	return this.selectList("TemplateDatasourceMapper.queryTemplateDatasourceList", templateDatasource);
    }
}



