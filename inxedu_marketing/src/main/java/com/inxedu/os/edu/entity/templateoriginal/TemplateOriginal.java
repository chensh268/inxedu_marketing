package com.inxedu.os.edu.entity.templateoriginal;

import java.io.Serializable;
import lombok.Data;
import java.util.*;

import java.io.Serializable;

/**
 * @author www.inxedu.com
 * @description 模板原始文件
 */
@Data
public class TemplateOriginal implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;

	/** id */
	private Long id;
	/** 模板名字 */
	private String name;
	/** 模板文件内容 */
	private String content;

}

